extends Node2D

@onready var music = [$Music_1, $Music_2, $Music_3, $Music_4, $Music_5, $Music_6, $Music_7, $Music_8, $Music_9 ]


func _ready():
	music[0].play()


func _on_Music_finished():
	music[randi() % music.size()].play()

extends CharacterBody2D
class_name Bullet

@export var vel := Vector2.ZERO

signal on_destroy


func destroy():
	emit_signal("on_destroy")
	queue_free()


func _physics_process(_delta):
	set_velocity(vel * -get_viewport_rect().size.y)
	move_and_slide()
	var _v = vel

extends Node2D


func _on_AudioStreamPlayer_finished():
	queue_free()


func _on_AnimatedSprite_animation_finished():
	$AnimatedSprite2D.visible = false
	$Explosion.visible = false

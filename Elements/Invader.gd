class_name Invader
extends CharacterBody2D

@export var bullet_prefab: Resource
@export var vel := Vector2.ZERO

signal is_shot


func _physics_process(_delta):
	set_velocity(vel * get_viewport_rect().size.x)
	move_and_slide()
	var _v = vel


func drop_bullet():
	var bullet = bullet_prefab.instantiate()
	bullet.global_position = global_position + Vector2(0, 20)
	
	return bullet


func destroy():
	emit_signal("is_shot", global_position)
	queue_free()


func _on_Area2D_body_entered(bullet):
	bullet.destroy()
	destroy()

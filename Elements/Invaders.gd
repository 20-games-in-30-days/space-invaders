extends Node2D

enum { DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_UP, DIRECTION_DOWN}

@export var invader_max_speed := 0.2
@export var max_shot_cooldown := 3
@export var min_shot_cooldown := 0.1
@export var drop_distance := 50
@export var grace_time := 5

var movement_direction := DIRECTION_RIGHT
var next_movement_direction := DIRECTION_RIGHT
var invader_velocity := Vector2.ZERO
var on_top = true
var resetting = false
var explosions = [preload("res://Elements/Explosion_0.tscn"), preload("res://Elements/Explosion_1.tscn"), preload("res://Elements/Explosion_2.tscn"), preload("res://Elements/Explosion_3.tscn")]
@onready var total_invaders := $Invader_Frame.get_child_count()
@onready var invader_count := $Invader_Frame.get_child_count()

signal invader_shot
signal last_invader_shot


func _ready():
	# Start the invaders moving.
	update_move_speed()
	
	# Start the shooting timer.
	$Shot_Timer.wait_time = max_shot_cooldown
	$Shot_Timer.start()


func update_move_speed():
	if invader_count > 0:
		var speed := invader_max_speed / invader_count
		
		match movement_direction:
			DIRECTION_LEFT:
				invader_velocity = Vector2(-speed, 0)
				
			DIRECTION_RIGHT:
				invader_velocity = Vector2(speed, 0)
				
			DIRECTION_DOWN:
				invader_velocity = Vector2(0, speed)
				
			DIRECTION_UP:
				invader_velocity = Vector2(0, -speed)
		
		for invader in $Invader_Frame.get_children():
			invader.vel = invader_velocity


func drop():
	if resetting:
		movement_direction = next_movement_direction
	else:
		movement_direction = DIRECTION_DOWN
		on_top = false
	
	# Get the lowest y value of any invader.
	var y_pos = 0
	for invader in $Invader_Frame.get_children():
		if (invader.global_position.y > y_pos):
			y_pos = invader.global_position.y
	
	$Drop.global_position.y = y_pos + drop_distance
	update_move_speed()


func reset():
	$Shot_Timer.start(grace_time)
	resetting = true
	
	if !on_top:
		if movement_direction != DIRECTION_DOWN:
			next_movement_direction = movement_direction
		movement_direction = DIRECTION_UP
		update_move_speed()


func _on_Top_body_entered(_body):
	on_top = true
	movement_direction = next_movement_direction
	update_move_speed()


func _on_Left_body_entered(_body):
	if next_movement_direction != DIRECTION_RIGHT:
		next_movement_direction = DIRECTION_RIGHT
		drop()


func _on_Right_body_entered(_body):
	if next_movement_direction != DIRECTION_LEFT:
		next_movement_direction = DIRECTION_LEFT
		drop()


func _on_Drop_body_entered(_body):
	movement_direction = next_movement_direction
	update_move_speed()


func _on_Invader_is_shot(pos: Vector2):
	invader_count -= 1
	
	var boom = explosions[randi() % explosions.size()].instantiate()
	boom.global_position = pos - Vector2(0, 25)
	add_child(boom)
	
	emit_signal("invader_shot")
	if invader_count == 0:
		emit_signal("last_invader_shot")
	else:
		update_move_speed()


func _on_Shot_Timer_timeout():
	# Also use this timer to reset when the player dies.
	resetting = false
	
	if invader_count > 0:
		# Select a random invader to take the shot.
		add_child($Invader_Frame.get_children()[randi() % invader_count].drop_bullet())
		
		var max_cooldown
		if invader_count == 1:
			max_cooldown = min_shot_cooldown * 2
		else:
			max_cooldown = lerp(min_shot_cooldown * 4, float(max_shot_cooldown), (invader_count / float(total_invaders)))
		
		# Select a random value that is more frequent when fewer invaders are left.
		$Shot_Timer.start(randf_range(min_shot_cooldown, max_cooldown))



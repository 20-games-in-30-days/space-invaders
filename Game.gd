extends CanvasLayer

@export var mothership_speed := 0.1
@export var mothership_spawn_timer := Vector2(2, 5)
@export var score := 0
@export var level := 0
@export var player_lives_left := 3
@export var levels := [] # (Array, Resource)


var explosions = [preload("res://Elements/Explosion_0.tscn"), preload("res://Elements/Explosion_1.tscn"), preload("res://Elements/Explosion_2.tscn"), preload("res://Elements/Explosion_3.tscn")]
var player_prefab = preload("res://Elements/Player.tscn")
var mothership_prefab = preload("res://Elements/Mothership.tscn")
var game_over := false
var in_level := false
var invaders
var player


func _ready():
	$Motherships/MothershipTimer.start()
	spawn_player()
	var _x = start_level()


func reset_game():
	level = 0
	player_lives_left = 3
	$Ui/Game_Text.visible = false
	game_over = false
	draw_player_lives()
	var _x = start_level()
	spawn_player()


func spawn_player():
	if player:
		player.queue_free()
	player = player_prefab.instantiate()
	add_child(player)
	player.connect("is_shot", Callable(self, "_on_Player_is_shot"))
	player.set_owner(self)


func spawn_mothership():
	var ship = mothership_prefab.instantiate()
	if randf_range(0, 1) < 0.5:
		ship.global_position = $Motherships/Mothership_L.global_position
		ship.vel = Vector2(0.1, 0)
	else:
		ship.global_position = $Motherships/Mothership_R.global_position
		ship.vel = Vector2(-0.1, 0)
	ship.connect("is_shot", Callable(self, "_on_Mothership_shot"))
	$Motherships.add_child(ship)


func add_score(value: int):
	score += value
	$Ui/Score.text = str(score)


func draw_player_lives():
	$Ui/Ship_1.visible = player_lives_left >= 1
	$Ui/Ship_2.visible = player_lives_left >= 2
	$Ui/Ship_3.visible = player_lives_left >= 3


# Creates the next level, or returns false if no more levels exist.
func start_level() -> bool:
	if level < levels.size():
		if invaders:
			invaders.queue_free()
		invaders = levels[level].instantiate()
		add_child(invaders)
		invaders.connect("invader_shot", Callable(self, "_on_Invaders_invader_shot"))
		invaders.connect("last_invader_shot", Callable(self, "_on_Invaders_last_invader_shot"))
		in_level = true
		return true
	else:
		return false


func _on_Scrubber_body_entered(bullet):
	if (bullet is Bullet):
		bullet.destroy()


func _on_MothershipTimer_timeout():
	if in_level:
		spawn_mothership()
	$Motherships/MothershipTimer.start(randf_range(mothership_spawn_timer.x, mothership_spawn_timer.y))


func _on_Mothership_shot(pos):
	for i in randf_range(5, 8):
		await get_tree().create_timer(0.1).timeout
		var boom = explosions[randi() % explosions.size()].instantiate()
		boom.global_position = pos + Vector2(randf_range(-75, 75), randf_range(-75, 75))
		add_child(boom)
	add_score(500)


func _on_Player_is_shot():
	player_lives_left -= 1
	player = null
	
	if player_lives_left >= 0:
		invaders.reset()
		for bullet in get_tree().get_nodes_in_group("Invader_Bullets"):
			bullet.queue_free()
		await get_tree().create_timer(5.0).timeout
		draw_player_lives()
		spawn_player()
	else:
		in_level = false
		game_over = true
		draw_player_lives()
		$Ui/Game_Text.text = "Game Over"
		$Ui/Game_Text.visible = true
		await get_tree().create_timer(5.0).timeout
		$Ui/Game_Text.text = "Press 'Shoot'\nTo Continue"


func _physics_process(_delta):
	if game_over and (Input.is_action_just_pressed("p_shoot")):
		reset_game()


func _on_Invaders_invader_shot():
	add_score(50)


func _on_Invaders_last_invader_shot():
	# Award a life for each level cleared
	if player_lives_left < 3:
		player_lives_left += 1
	else:
		# Level clear bonus!
		add_score(5000)
		
	level += 1
	$Ui/Game_Text.text = "Wave " + str(level) + " Cleared"
	$Ui/Game_Text.visible = true
	in_level = false
	await get_tree().create_timer(3.0).timeout
	draw_player_lives()
	$Ui/Game_Text.visible = false
	
	if not start_level():
		# We hit the last level.
		game_over = true
		$Ui/Game_Text.text = "VICTORY!"
		$Ui/Game_Text.visible = true
		await get_tree().create_timer(5.0).timeout
		$Ui/Game_Text.text = "Press 'Shoot'\nTo Continue"


func _on_Ground_body_entered(_body):
	player_lives_left = 0
	if player:
		player.is_shot()
	
